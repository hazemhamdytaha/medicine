import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Global} from "../service/global";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";
import {GlobalFooService} from "../service/GlobalFooService";

@Component({
  selector: 'app-init',
  templateUrl: './init.page.html',
  styleUrls: ['./init.page.scss'],
})
export class InitPage implements OnInit {

  constructor( private router: Router ,
               private navCTRL: NavController ,
               private toast: ToastCTRL ,
               private api: Api ,
               public gloable: Global ,
               private globalFooService: GlobalFooService,
               private loadingCTRL: LoadingService ,
               private storage: Storage) { }

  ngOnInit() {

  }

  ionViewDidEnter() {
    // this.globalFooService.publishSomeData({
    //   foo: 'bar'
    // });
    console.log('this is login init ===> ', this.gloable.login);
    // if(this.gloable.login == '1'){
      this.navCTRL.navigateForward('tabs/tab1');
    // }else{
    //   this.navCTRL.navigateForward('login');
    // }
  }

}
