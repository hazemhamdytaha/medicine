import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Global} from "../service/global";
import {GlobalFooService} from "../service/GlobalFooService";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {}

}
