import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ModalController, NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  public register: FormGroup;
  form = "form";

  validation_messages = {
    'email': [
      { type: 'pattern', message: 'يجب ان يكون بصيغة بريد اللكتروني' }
    ],
    'phone': [],
    'user_name': [],
    'name': [
      // {type: 'required', message: '*'},
      {type: 'pattern', message: 'Please enter a valid Name'}
    ],
    'password': [
      {type: 'pattern', message: 'Password must be one uppercase and lowercase letter, and at least 8 or more characters '}
    ]

  };

  constructor(  private formBuilder: FormBuilder,
                private router: Router ,
                private navCTRL: NavController ,
                private toast: ToastCTRL ,
                private api: Api ,
                public lang: Lang ,
                private  roters: ActivatedRoute,
                public gloable: Global ,
                private loadingCTRL: LoadingService ,
                private storage: Storage,
                public modalCtrl: ModalController ) {



    this.register = this.formBuilder.group({
      user_name: ['', Validators.required],
      password: new FormControl('', Validators.compose([
        Validators.required,
        // Validators.pattern('(?=.*[a-z])(?=.*[A-Z]).{8,}')
      ]))
    });
  }
  ionViewWillEnter() {

    if (document.documentElement.dir == 'rtl') {

      this.form = "form";

    } else {

      this.form = "form-en";

    }
  }

  logForm(){
    this.loadingCTRL.present();
    console.log(this.register.value);
    this.api.customerLogin(this.register.value).then((data:any)=>{
      this.loadingCTRL.dismiss();
      if(data.status == '1'){
        this.toast.simple(this.lang.mainlang.register.messagesuc);
        this.storage.set("login",'1');
        this.gloable.login = '1';
        this.storage.set("user_id" , data.user_id);
        this.gloable.user_id = data.user_id ;
        this.navCTRL.navigateForward("init");
      }else{
        this.toast.simple(this.lang.mainlang.register.messageerror);
      }
    });
  }

  goToHome(){
    this.router.navigateByUrl('/home');
  }

  goToLogin(){
    this.router.navigateByUrl('/login');
  }

newegoster(){
  this.router.navigateByUrl('/register');

}
back(){
    this.navCTRL.back();
}
  ngOnInit() {
  }

}
