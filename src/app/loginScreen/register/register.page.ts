import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalController, NavController} from '@ionic/angular';
import {ToastCTRL} from '../../service/toastCTRL';
import {Api} from '../../service/api';
import {Global} from '../../service/global';
import {LoadingService} from '../../service/loadingService';
import {Storage} from '@ionic/storage';
import {Lang} from "../../service/lang";


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage  {


 public register : FormGroup;
 form = "form";

 validation_messages = {
   'username':[],
   'mobile':[],
  'email': [
    { type: 'pattern', message: 'يجب ان يكون بصيغة بريد اللكتروني' }
  ],
  'phone': [],
   'name': [
     // {type: 'required', message: '*'},
     {type: 'pattern', message: 'Please enter a valid Name'}
   ],
   'password': [
       {type: 'pattern', message: 'Password must be one uppercase and lowercase letter, and at least 8 or more characters '}
   ]

};

 constructor( private formBuilder: FormBuilder,
              private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang ,
              private  roters: ActivatedRoute,
              public gloable: Global ,
              private loadingCTRL: LoadingService ,
              private storage: Storage,
              public modalCtrl: ModalController ) {



  this.register = this.formBuilder.group({
    name: ['', Validators.required],
    username: ['', Validators.required],
    email: ['', Validators.required],
    mobile: ['', Validators.required],
    phone: ['', Validators.required],
    password: new FormControl('', Validators.compose([
      Validators.required,
      // Validators.pattern('(?=.*[a-z])(?=.*[A-Z]).{8,}')
    ]))
  });
}
ionViewWillEnter() {

  if (document.documentElement.dir == 'rtl') {

    this.form = "form";

  } else {

    this.form = "form-en";

  }
}

logForm(){
   this.loadingCTRL.present();
  console.log(this.register.value);
  this.register.value.phone_qid = this.gloable.uinqid ;
  console.log(this.register.value);
  this.api.customerRegister(this.register.value).then((reg:any)=>{
    this.loadingCTRL.dismiss();
    if (reg.status == '1'){
      this.toast.simple(this.lang.mainlang.register.messagesuc);
      this.navCTRL.navigateForward('login');
    }else if (reg.status == '4'){
      this.toast.simple(this.lang.mainlang.register.messagesuc4);
      // this.navCTRL.navigateForward('login');
    }else if (reg.status == '5'){
      this.toast.simple(this.lang.mainlang.register.messagesuc5);
      // this.navCTRL.navigateForward('login');
    }else if (reg.status == '6'){
      this.toast.simple(this.lang.mainlang.register.messagesuc6);
      // this.navCTRL.navigateForward('login');
    }else {
        this.toast.simple(this.lang.mainlang.register.messageerror);
      }

  });
}
  // 4 username
  // 5 email
  // 6 mobile
goToHome(){
  this.router.navigateByUrl('/home');
}

goToLogin(){
  this.router.navigateByUrl('/login');
}


}
