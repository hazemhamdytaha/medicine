import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ImgFallbackModule} from 'ngx-img-fallback';


@NgModule({
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
  imports: [
    IonicModule,
      ImgFallbackModule,
  ],
  declarations: [
      // TimeAgoPipe

  ],
  exports: [
      ImgFallbackModule,


  ]
})

export class SharedModule { }
