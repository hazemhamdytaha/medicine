import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'init',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./mainScreen/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'order-detail',
    loadChildren: () => import('./mainScreen/order-detail/order-detail.module').then( m => m.OrderDetailPageModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./mainScreen/products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./mainScreen/product-detail/product-detail.module').then( m => m.ProductDetailPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./loginScreen/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./generalScreen/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'more',
    loadChildren: () => import('./mainScreen/more/more.module').then( m => m.MorePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./loginScreen/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'init',
    loadChildren: () => import('./init/init.module').then( m => m.InitPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'addressadd',
    loadChildren: () => import('./mainScreen/addressadd/addressadd.module').then( m => m.AddressaddPageModule)
  },
  {
    path: 'adressadd',
    loadChildren: () => import('./mainScreen/adressadd/adressadd.module').then( m => m.AdressaddPageModule)
  },
  {
    path: 'my-address',
    loadChildren: () => import('./mainScreen/my-address/my-address.module').then( m => m.MyAddressPageModule)
  },
  {
    path: 'edit',
    loadChildren: () => import('./mainScreen/edit/edit.module').then( m => m.EditPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
