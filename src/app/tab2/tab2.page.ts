import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Lang} from "../service/lang";
import {Global} from "../service/global";
import {GlobalFooService} from "../service/GlobalFooService";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
all:any;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {}
    ionViewDidEnter() {
      if(this.gloable.login != '1'){
        this.navCTRL.navigateBack('login');
        this.toast.simple(this.lang.mainlang.tabs.message);
      }
    this.loadingCTRL.present();
this.api.customerOrders({customer_id:this.gloable.user_id}).then(order=>{
  this.all = order ;
  this.loadingCTRL.dismiss();
});
  }



  goToOrderDetail(id){
    this.router.navigateByUrl('/order-detail;id=' + id);
  }

  goToMore(){
    // this.navCTRL.navigateForward('more');
    this.gloable.more();
  }
  goBack(){
    this.router.navigateByUrl('/tabs/tab1');
  }

  goToCart(){
    this.router.navigateByUrl('/cart');
  }

}
