import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Lang} from "../service/lang";
import {Global} from "../service/global";
import {GlobalFooService} from "../service/GlobalFooService";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
all:any;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {
    if(this.gloable.login != '1'){
      this.navCTRL.navigateBack('login');
      this.toast.simple(this.lang.mainlang.tabs.message);
    }
    this.loadingCTRL.present();
    this.api.customerFavourites({customer_id:this.gloable.user_id}).then( fav => {
      this.all = fav ;
      this.loadingCTRL.dismiss();
    });
  }

  removefav(id){

    this.loadingCTRL.present();
    this.api.customerRemoveFavourite({id:id}).then((data:any)=>{
      if(data.status == '1'){
        this.toast.simple('favorite removed');
        this.api.customerFavourites({customer_id:this.gloable.user_id}).then( fav => {
          this.all = fav ;
          this.loadingCTRL.dismiss();
        });
      }else{
        this.toast.simple('removed failed');
        this.loadingCTRL.dismiss();
      }
    });
  }
  goToMore(){
    // this.navCTRL.navigateForward('more');
    this.gloable.more();
  }
  goMore(){
    this.gloable.more();
  }
  goToCart(){
    this.router.navigateByUrl('/cart');
  }
}
