import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Global} from "../service/global";
import {GlobalFooService} from "../service/GlobalFooService";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";
import {Lang} from "../service/lang";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
all:any;
all1:any;
  allfav:any;
   allprod:any;
name:any;
   alls:any;
  offset= 0 ;
  pageLoad = '10';
  count=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {

    this.loadingCTRL.present();
this.api.products({client_id:this.gloable.user_id ,offset:this.offset,pageLoad:this.pageLoad }).then((prod:any)=>{
  this.all = prod ;
  console.log('ddd=>', this.all);
  this.loadingCTRL.dismiss();

});
    this.api.products({client_id:this.gloable.user_id,order_type:3,pageLoad:5}).then((prod:any)=>{
      this.allprod = prod ;
      console.log('ddd=>', this.all);
    });
    this.api.sliders({}).then((slier:any)=>{
      this.alls = slier ;
      console.log('d٣٣dd======>', this.alls);
    });
  }



  addcart(id,i){
    this.api.customerAddEditCart({customer_id:this.gloable.user_id ,product_id:id ,amount:this.count[i]}).then((cart:any)=>{
      if(cart.status == '1'){
        this.toast.simple('ADD TO CART DONE');
        this.navCTRL.navigateForward('cart');
        this.count[i] = 1 ;
      }else{
        this.toast.simple('Error');
      }
    });
  }

  search(){
    this.loadingCTRL.present();
    this.api.products({client_id:this.gloable.user_id ,search_name: this.name}).then((prod:any)=>{
      this.all = prod ;
      console.log('ddd=>', this.all);
      this.loadingCTRL.dismiss();
    });
  }
  goToProductDetail(id){
    this.router.navigateByUrl('/product-detail;id=' + id);
  }

  goMore(){
    this.router.navigateByUrl('/more');
  }
  goToCart(){
    this.router.navigateByUrl('/cart');
  }
  inc(i){
    this.count[i] = this.count[i] + 1 ;
    console.log(this.count);
  }
  dec(i){
    if(this.count[i] <= 1){

    }else{
      this.count[i] = this.count[i] - 1 ;
      console.log(this.count);


    }
  }
  addfav(id , i){
    this.toast.simple1('add to favorite');
    this.all[i].is_favourite = 1;
    this.api.customerAddFavourite({customer_id:this.gloable.user_id,product_id:id}).then(fav=>{
      this.allfav = fav ;

    });

  }
  goToMore(){
    // this.navCTRL.navigateForward('more');
    this.gloable.more();
  }
  removetofava(id,i){
    this.api.customerRemoveFavourite({id:id});
    this.all.is_favourite = 0;
    this.all[i].is_favourite = 0 ;
    this.toast.simple1(this.lang.mainlang.products.removeSuccess);
  }


  loadData(event) {
    // setTimeout(() => {
    //   console.log('Done');
    //
    //   // App logic to determine if all data is loaded
    //   // and disable the infinite scroll
    //   // if (data.length == 1000) {
    //   //   event.target.disabled = true;
    //   // }
    // }, 500);
    console.log('before=>' , this.offset);
this.offset += 10 ;
    console.log('after=>' , this.offset);

    this.api.products({client_id: this.gloable.user_id , offset: this.offset, pageLoad: this.pageLoad }).then((prod:any)=>{
      // this.all += prod ;
      this.all = this.all.concat(prod);
      console.log('all dddd  =>', this.all);
      event.target.complete();
    });
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

}
