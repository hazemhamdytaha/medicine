import { Component, OnInit } from '@angular/core';
import {ModalController, NavController} from "@ionic/angular";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-more',
  templateUrl: './more.page.html',
  styleUrls: ['./more.page.scss'],
})
export class MorePage implements OnInit {

  constructor( private router: Router ,
               private navCTRL: NavController ,
               private toast: ToastCTRL ,
               private api: Api ,
               public lang: Lang ,
               private  roters: ActivatedRoute,
               public gloable: Global ,
               private loadingCTRL: LoadingService ,
               private storage: Storage,
               public modalCtrl: ModalController ) { }

  ngOnInit() {
  }
  back(){
    this.navCTRL.back();
  }
  gotourl(url){
    this.modalCtrl.dismiss();

    if(url == 'logout'){
this.storage.clear();
this.gloable.login  =0 ;
this.modalCtrl.dismiss();
this.navCTRL.navigateForward('init');
    }else{
      this.navCTRL.navigateForward(url);
    }
  }
  changelang(id) {
    // this.gloable.language_id = id;
    console.log(this.gloable.language_id);
    if(id =='2' ){
      document.documentElement.dir = 'ltr';
      this.lang.mainlang = this.lang.en ;
      this.gloable.language_id = 2;
      this.storage.set('lang','2');
    }else{
      document.documentElement.dir = 'rtl';
      this.lang.mainlang = this.lang.ar ;
      this.gloable.language_id = 1;
      this.storage.set('lang','1');
    }
  }

}
