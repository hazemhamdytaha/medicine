import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyAddressPageRoutingModule } from './my-address-routing.module';

import { MyAddressPage } from './my-address.page';
import {SharedModule} from "../../shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      SharedModule,
    IonicModule,
    MyAddressPageRoutingModule
  ],
  declarations: [MyAddressPage]
})
export class MyAddressPageModule {}
