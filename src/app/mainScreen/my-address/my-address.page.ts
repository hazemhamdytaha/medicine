import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AlertController, NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-my-address',
  templateUrl: './my-address.page.html',
  styleUrls: ['./my-address.page.scss'],
})
export class MyAddressPage implements OnInit {
all:any;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              private alertController: AlertController,
              public gloable: Global ,
              private loadingCTRL: LoadingService ,
              private storage: Storage ) {
    this.api.customerAddresses({customer_id:this.gloable.user_id}).then(data=>{
      this.all = data;
    });
  }

  ngOnInit() {
  }
  goToEditAddress(){}
  remove(id) {
    this.api.customerDelAddress({id: id}).then(data => {
      this.toast.simple(this.lang.mainlang.adressadd.sucessmessage);
      this.api.customerAddresses({customer_id:this.gloable.user_id}).then(data=>{
        this.all = data;
      });
    });
  }
  back(){
    this.navCTRL.back();
  }
}
