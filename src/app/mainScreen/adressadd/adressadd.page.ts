import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AlertController, NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-adressadd',
  templateUrl: './adressadd.page.html',
  styleUrls: ['./adressadd.page.scss'],
})
export class AdressaddPage implements OnInit {
data ={
  client_id:'' ,
  address_first_name:'' ,
  address_last_name:'',
  address_company:'',
  address:'',
  address_address_2:'',
  mainaddress:'',
  shipping_method_id:'1',
  address_city:'',
  address_post_code:'',
  country_id:'1',
  region:'',
  is_default:'',

};
regins:any;
type:any;
counties:any;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              private alertController: AlertController,
              public gloable: Global ,
              private roters: ActivatedRoute,
              private loadingCTRL: LoadingService ,
              private storage: Storage ) {

    this.loadingCTRL.present();
    this.type = this.roters.snapshot.paramMap.get('type');
    this.api.countries({}).then(country=>{
      this.counties  = country ;
      this.loadingCTRL.dismiss();
    });
    // this.api.regions({country_id:'2',language_id:this.gloable.language_id}).then(regin=>{
    //      this.regins = regin;
    //    });
        }

  ngOnInit() {
  }
  add(){
    this.data.client_id = this.gloable.user_id ;
    this.loadingCTRL.present();
    console.log(this.data);
    this.api.customerNewAddress(this.data).then((done:any)=>{
      if(done.status =='1'){
        this.toast.simple(this.lang.mainlang.adressadd.messagesucess);
        this.loadingCTRL.dismiss();
        if(this.type == '-1'){
          this.navCTRL.back();
        }
      }else{
        this.toast.simple(this.lang.mainlang.adressadd.messagefail);
        this.loadingCTRL.dismiss();
      }
    });
  }
back(){
    this.navCTRL.back();
}
}
