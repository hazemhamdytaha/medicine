import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdressaddPage } from './adressadd.page';

describe('AdressaddPage', () => {
  let component: AdressaddPage;
  let fixture: ComponentFixture<AdressaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdressaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdressaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
