import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdressaddPage } from './adressadd.page';

const routes: Routes = [
  {
    path: '',
    component: AdressaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdressaddPageRoutingModule {}
