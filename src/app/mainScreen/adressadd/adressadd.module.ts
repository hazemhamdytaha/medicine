import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdressaddPageRoutingModule } from './adressadd-routing.module';

import { AdressaddPage } from './adressadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdressaddPageRoutingModule
  ],
  declarations: [AdressaddPage]
})
export class AdressaddPageModule {}
