import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Global} from "../../service/global";
import {GlobalFooService} from "../../service/GlobalFooService";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  all:any;
allfav:any;
count=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
  // customer_id4403
  // product_id301
  // amount5
  // cart_id3
  offset= 0 ;
  pageLoad = '10';
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {
    this.loadingCTRL.present();
this.api.products({offset:this.offset,pageLoad:this.pageLoad }).then(prod => {
  this.all = prod ;
  this.loadingCTRL.dismiss();
});

  }
  addcart(id,i){
    this.api.customerAddEditCart({customer_id:this.gloable.user_id ,product_id:id ,amount:this.count[i]}).then((cart:any)=>{
      if(cart.status == '1'){
        this.toast.simple('ADD TO CART DONE');
        this.navCTRL.navigateForward('cart');
        this.count[i] = 1 ;
      }else{
        this.toast.simple('Error');
      }
    });
  }

  goToProductDetail(id){
    this.router.navigateByUrl('/product-detail;id=' + id);
  }

  goMore(){
    this.router.navigateByUrl('/more');
  }
  goToCart(){
    this.router.navigateByUrl('/cart');
  }
  inc(i){
    this.count[i] = this.count[i] + 1 ;
    console.log(this.count);
  }
  dec(i){
    if(this.count[i] <= 1){

    }else{
      this.count[i] = this.count[i] - 1 ;
      console.log(this.count);


    }
  }
  addfav(id , i){
    this.api.customerAddFavourite({customer_id:this.gloable.user_id,product_id:id}).then(fav=>{
      this.allfav = fav ;
    });

  }
  goToMore(){
    this.gloable.more();
  }


  loadData(event) {
    // setTimeout(() => {
    //   console.log('Done');
    //
    //   // App logic to determine if all data is loaded
    //   // and disable the infinite scroll
    //   // if (data.length == 1000) {
    //   //   event.target.disabled = true;
    //   // }
    // }, 500);
    console.log('before=>' , this.offset);
    this.offset += 10 ;
    console.log('after=>' , this.offset);

    this.api.products({client_id: this.gloable.user_id , offset: this.offset, pageLoad: this.pageLoad }).then((prod:any)=>{
      // this.all += prod ;
      this.all = this.all.concat(prod);
      console.log('all dddd  =>', this.all);
      event.target.complete();
    });
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

}
