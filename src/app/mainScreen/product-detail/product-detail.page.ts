import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Global} from "../../service/global";
import {GlobalFooService} from "../../service/GlobalFooService";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";
import {Lang} from "../../service/lang";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage  {
all:any;
id:any;
  allfav:any;
  count=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
  constructor( private router: Router ,
               private navCTRL: NavController ,
               private toast: ToastCTRL ,
               private api: Api ,
               public gloable: Global ,
               private roters: ActivatedRoute,
               private lang: Lang,

               private globalFooService: GlobalFooService,
               private loadingCTRL: LoadingService ,
               private storage: Storage){
    this.loadingCTRL.present();
    this.id = this.roters.snapshot.paramMap.get('id');

this.api.products({id:this.id ,client_id:this.gloable.user_id}).then((data:any)=>{
  this.all = data[0];
  this.loadingCTRL.dismiss();
});
  }
  goToProducts(){
    this.router.navigateByUrl('/products');
  }
  goToCart(){
    this.router.navigateByUrl('/cart');
  }

  back(){
    this.navCTRL.back();
  }
  addfav(id , i){
    this.all.is_favourite = 1;
    this.toast.simple('add to favorite');
    this.api.customerAddFavourite({customer_id:this.gloable.user_id,product_id:id}).then(fav=>{
      this.allfav = fav ;
    });

  }

  inc(i){
    this.count[i] = this.count[i] + 1 ;
    console.log(this.count);
  }
  dec(i){
    if(this.count[i] <= 1){

    }else{
      this.count[i] = this.count[i] - 1 ;
      console.log(this.count);


    }
  }
  goToProductDetail(id){
    this.router.navigateByUrl('/product-detail;id=' + id);
  }
  removetofava(id){
    this.api.customerRemoveFavourite({id:id});
    this.all.is_favourite = 0;
    this.toast.simple(this.lang.mainlang.products.removeSuccess);
  }
  addcart(id,i){
    this.api.customerAddEditCart({customer_id:this.gloable.user_id ,product_id:id ,amount:this.count[1]}).then((cart:any)=>{
      if(cart.status == '1'){
        this.toast.simple('ADD TO CART DONE');
        this.navCTRL.navigateForward('cart');
        this.count[i] = 1 ;
      }else{
        this.toast.simple('Error');
      }
    });
  }

}
