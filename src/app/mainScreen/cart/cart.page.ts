import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Global} from "../../service/global";
import {GlobalFooService} from "../../service/GlobalFooService";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";
import {Lang} from "../../service/lang";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage  {
all:any;
  allfav:any;
  count=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public gloable: Global ,
              public lang: Lang ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {}
  ionViewDidEnter() {
    if(this.gloable.login != '1'){
      this.navCTRL.navigateBack('login');
      this.toast.simple(this.lang.mainlang.tabs.message);
    }
    this.loadingCTRL.present();
this.api.customerCart({customer_id:this.gloable.user_id}).then(cart=>{
  this.all = cart ;
  let i = 0 ;
  for (let x of this.all.items){
    this.count[i] = x.amount ;
    i++;
  }
  this.loadingCTRL.dismiss();
  // console.log()
});

  }

  addcart(id,i){
    this.loadingCTRL.present();
    this.api.customerAddEditCart({customer_id:this.gloable.user_id ,product_id:id ,amount:i}).then((cart:any)=>{
      if(cart.status == '1'){
        this.toast.simple('ADD TO CART DONE');
        this.navCTRL.navigateForward('cart');
        this.count[i] = 1 ;
        this.api.customerCart({customer_id:this.gloable.user_id}).then(cart=>{
          this.all = cart ;
          this.loadingCTRL.dismiss();

        });
      }else{
        this.toast.simple('Error');
        this.loadingCTRL.dismiss();
      }
    });
  }

  goToProductDetail(id){
    this.router.navigateByUrl('/product-detail;id=' + id);
  }

  goMore(){
    // this.router.navigateByUrl('/more');
    this.gloable.more();

  }
  goToCart(){
    this.router.navigateByUrl('/cart');
  }
  inc(i,id){
    this.count[i] = this.count[i] + 1 ;
    console.log(this.count);
    this.addcart(id,this.count[i]);

  }
  dec(i,id){
    if(this.count[i] <= 1){

    }else{
      this.count[i] = this.count[i] - 1 ;
      console.log(this.count);


    }
  }
  addfav(id , i){
    this.api.customerAddFavourite({customer_id:this.gloable.user_id,product_id:id}).then(fav=>{
      this.allfav = fav ;
    });

  }
  remove(id){
    this.loadingCTRL.present();
    this.api.customerRemoveCart({id:id}).then(data=>{
      this.api.customerCart({customer_id:this.gloable.user_id}).then(cart=>{
        this.all = cart ;
        this.loadingCTRL.dismiss();

      });
    });
  }
  chekout(){
    this.navCTRL.navigateForward('checkout');
  }

}
