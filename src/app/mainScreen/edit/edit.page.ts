import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ModalController, NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  public register : FormGroup;
  form = "form";

  validation_messages = {
    'username':[],
    'mobile':[],
    'email': [
      { type: 'pattern', message: 'يجب ان يكون بصيغة بريد اللكتروني' }
    ],
    'phone': [],
    'name': [
      // {type: 'required', message: '*'},
      {type: 'pattern', message: 'Please enter a valid Name'}
    ],
    'password': [
      {type: 'pattern', message: 'Password must be one uppercase and lowercase letter, and at least 8 or more characters '}
    ]

  };
all:any;
  constructor( private formBuilder: FormBuilder,
               private router: Router ,
               private navCTRL: NavController ,
               private toast: ToastCTRL ,
               private api: Api ,
               public lang: Lang ,
               private  roters: ActivatedRoute,
               public gloable: Global ,
               private loadingCTRL: LoadingService ,
               private storage: Storage,
               public modalCtrl: ModalController ) {
this.loadingCTRL.present();


    this.register = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      phone: ['', Validators.required],
      password: new FormControl('', Validators.compose([
        // Validators.required,
        // Validators.pattern('(?=.*[a-z])(?=.*[A-Z]).{8,}')
      ]))
    });
    // id: "5976"
    // mobile: "01110070881"
    // name: "new"
    // phone: "01110070881"
    // s_id: 0
    // user_active: "1"
    // user_email: "hamd@sss.com"
    // user_name: "new"
    // user_password_hash: "$2y$10$NxAQHdlJK5qG5fsFjdb3TOXeGyFURbYtKfTzwC5R52zBYutquHuaq"
    // user_registration_datetime: "2021-05-20 18:49:09"
     this.api.customerProfile({id:this.gloable.user_id}).then((prof:any)=>{
       this.all = prof ;
       this.register.controls['name'].setValue(prof.name);
       this.register.controls['username'].setValue(prof.user_name);
       this.register.controls['email'].setValue(prof.user_email);
       this.register.controls['mobile'].setValue(prof.mobile);
       this.register.controls['phone'].setValue(prof.phone);
       // this.register.controls['password'].setValue(prof.name);
       this.loadingCTRL.dismiss();
     });
  }
  ionViewWillEnter() {

    if (document.documentElement.dir == 'rtl') {

      this.form = "form";

    } else {

      this.form = "form-en";

    }
  }

  logForm(){
    this.loadingCTRL.present();
    console.log(this.register.value);
    this.register.value.id = this.gloable.user_id ;
    console.log(this.register.value);
    this.api.customerUpdateProfile(this.register.value).then((reg:any)=>{
      this.loadingCTRL.dismiss();
      if (reg.status == '1'){
        this.toast.simple(this.lang.mainlang.register.messagesuc);
        // this.navCTRL.navigateForward('login');
      }else{
        this.toast.simple(this.lang.mainlang.register.messageerror);
      }
    });
  }

  goToHome(){
    // this.router.navigateByUrl('/home');
    this.gloable.more();

  }

  goToLogin(){
this.navCTRL.back()
  }



  ngOnInit() {
  }

}
