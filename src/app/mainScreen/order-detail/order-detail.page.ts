import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from "@ionic/angular";
import {ToastCTRL} from "../../service/toastCTRL";
import {Api} from "../../service/api";
import {Lang} from "../../service/lang";
import {Global} from "../../service/global";
import {GlobalFooService} from "../../service/GlobalFooService";
import {LoadingService} from "../../service/loadingService";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage  {
all:any;
id:any;
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              public lang: Lang,
              private roters: ActivatedRoute,
              public gloable: Global ,
              private globalFooService: GlobalFooService,
              private loadingCTRL: LoadingService ,
              private storage: Storage) {}
  ionViewDidEnter() {
    this.loadingCTRL.present();
    this.id = this.roters.snapshot.paramMap.get('id');

    this.api.customerOrders({customer_id:this.gloable.user_id,id:this.id}).then(order=>{
      this.all = order ;
      this.loadingCTRL.dismiss();
    });
  }




  goToBack(){
    this.router.navigateByUrl('/tabs/tab2');
  }

}
