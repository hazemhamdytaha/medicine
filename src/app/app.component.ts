import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Lang } from './service/lang';
import {Storage} from '@ionic/storage';
import {Global} from "./service/global";
import {GlobalFooService} from "./service/GlobalFooService";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    public lang: Lang,
    public globalFooService: GlobalFooService,
    public gloable: Global,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.firstrun();
      this.globalFooService.getObservable().subscribe((data) => {
        console.log('Data received============>', data);
        this.firstrun();
      });
    });
  }
  firstrun(){
    this.storage.get('lang').then(lang=>{
      if((lang != '1') ){
        this.lang.mainlang = this.lang.en ;
        this.gloable.language_id = 2;
        document.documentElement.dir = 'ltr';
        console.log('this is english lang',this.lang.mainlang) ;
      }else{
        document.documentElement.dir = 'rtl';
        this.lang.mainlang = this.lang.ar ;
        this.gloable.language_id = 1;

        console.log('this is arabic lang' ,this.lang.mainlang) ;
      }
    });
    this.storage.get('login').then((login:any)=>{
      console.log('loggggin value =====>' , login);
      if(login == '1'){
        this.gloable.login = '1';
        this.storage.get('user_id').then((user_id:any)=>{
          console.log('loggggin value =====>' , user_id);
          this.gloable.user_id = user_id ;
        });
      }else{
        this.gloable.login = '0';
      }
    });

    // this.storage.get('currency_id').then((currency_id:any)=>{
    //   console.log('currency_id value =====>' , currency_id);
    //   if(currency_id == null){
    //     this.gloable.currency_id = 1 ;
    //   }else{
    //     this.gloable.currency_id = currency_id ;
    //   }
    // });
  }
  changelang(id){
    this.gloable.language_id = id;
    console.log(this.gloable.language_id);
    // this.storage.get('lang').then(lang=>{
    //   if((lang != 'ar') ){
    //     this.lang.mainlang = this.lang.en ;
    //     this.gloable.language_id = 2;
    //     document.documentElement.dir = 'ltr';
    //     console.log('this is english lang',this.lang.mainlang) ;
    //   }else{
    //     document.documentElement.dir = 'rtl';
    //     this.lang.mainlang = this.lang.ar ;
    //     this.gloable.language_id = 1;
    //
    //     console.log('this is arabic lang' ,this.lang.mainlang) ;
    //   }
    // });
  }
}
