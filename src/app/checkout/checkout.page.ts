import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AlertController, NavController} from "@ionic/angular";
import {ToastCTRL} from "../service/toastCTRL";
import {Api} from "../service/api";
import {Lang} from "../service/lang";
import {Cartservice} from "../service/cartservice";
import {Global} from "../service/global";
import {LoadingService} from "../service/loadingService";
import {Storage} from "@ionic/storage";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  all:any ;
  url:any;
  all1:any ;
  cart:any;
  reid:any;
  placeholder = '../../../assets/img/New Project (8).png';
data={customer_id:'',address_id:'',pay_type:'',language_id:'',seller_id:''};
  constructor(private router: Router ,
              private navCTRL: NavController ,
              private toast: ToastCTRL ,
              private api: Api ,
              private iab: InAppBrowser,
              public lang: Lang,
              private cart1: Cartservice,
              private roters: ActivatedRoute,
              private alertController: AlertController,
              public gloable: Global ,
              private loadingCTRL: LoadingService ,
              private storage: Storage) { }

  ionViewDidEnter() {
    this.reid = this.roters.snapshot.paramMap.get('id');

    this.api.customerAddresses({customer_id:this.gloable.user_id}).then(data=>{
      this.all = data ;
    });
  }
  ngOnInit() {
  }
  back(){
    this.navCTRL.back();
  }
  checkout(){
    this.loadingCTRL.present();
    this.data.customer_id = this.gloable.user_id;
    this.data.language_id = this.gloable.language_id ;
    this.data.pay_type = '1';
    this.data.seller_id = this.reid;
    this.api.customerNewOrder(this.data).then((order:any)=>{
      if(order.status == '1'){
        // this.toast.simple(this.lang.mainlang.adressadd.sucessmessage);
        console.log(order.response.Data.PaymentURL);
        this.excute(order.response.Data.PaymentURL);
      }else{
        this.toast.simple(this.lang.ar.adressadd.messagefail);
      }
    });
    console.log(this.data);
  }

  adress(event){
    console.log(event);
    if(event.target.value == '-1'){
      this.navCTRL.navigateForward('adressadd;type=-1');
    }else {
      this.loadingCTRL.present();
      this.api.customerAddresses({customer_id: this.gloable.user_id, id: event.target.value}).then(data => {
        this.all1 = data;
        this.loadingCTRL.dismiss();
      });
    }
  }


  excute(url){

    const browser = this.iab.create(url);
    browser.on('loadstop').subscribe(
        (data:any) => {
          console.log(data.url);
          console.log(data.url.slice(0,35));
          if( data.url.slice(0,35) == 'https://evently.sa/web/cart/success'){
            console.log(data.url.slice(46, data.url.length));
            browser.close();
            this.navCTRL.navigateForward('sucess;type=1;id=' + data.url.slice(46, data.url.length) ) ;
          }else if(data.url.slice(0,33) == 'https://evently.sa/web/cart/error'){
            browser.close();
            this.navCTRL.navigateForward('sucess;type=0;id=' + data.url.slice(46, data.url.length) ) ;
            console.log('error contact admin');
          }
        });

  }

}
