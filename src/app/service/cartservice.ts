import {ApplicationRef, Injectable} from '@angular/core';
import {AlertController, LoadingController, NavController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer/ngx";
import {File} from "@ionic-native/file/ngx";
import {ToastCTRL} from "./toastCTRL";
import {Api} from "./api";
import {Lang} from "./lang";
import {Global} from "./global";
import {LoadingService} from "./loadingService";
import {Storage} from "@ionic/storage";

@Injectable({
    providedIn: 'root'
})
export class Cartservice {

    constructor(private router: Router ,
                private navCTRL: NavController ,
                private toast: ToastCTRL ,
                private api: Api ,
                private lang: Lang,
                private roters: ActivatedRoute,
                private alertController: AlertController,
                private gloable: Global ,
                private loadingCTRL: LoadingService ,
                private storage: Storage,
    ) {
    }
    public cartadd(customer_id,product_id,amount,size?,color?,cart_id?){
        return new Promise((resolve, reject) => {

                this.api.customerAddEditCart({
                customer_id: customer_id,
                product_id: product_id,
                amount: amount,
                size_id: size,
                color_id: color,
                cart_id: cart_id
            }).then(cart => {
                resolve(cart);
            }).catch(error=>{
                reject(error);
            });
        });
    }
    }
