import {Injectable} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {MorePage} from "../mainScreen/more/more.page";

@Injectable({
  providedIn: 'root'
})

export class  Global {
  constructor(public modalCtrl: ModalController) {
  }
  public type: any ;
  public data: any;
  public language_id: any;
  public currency_id: any;
  public uinqid: any;
  public sendata: any;
  public login: any;
  public patient_id: any;
  public doctor_id: any;
  public user_id: any;
  public async more(){

    // this.client.disconnect();
    const modal = await this.modalCtrl.create({
      component: MorePage,
      cssClass: 'my-custom-class',
      componentProps: {
        // 'id': this.id,
        // 'multi': 0
      }
    });
    modal.onDidDismiss()
      .then((data:any) => {

      });

    return await modal.present();
  }

}
