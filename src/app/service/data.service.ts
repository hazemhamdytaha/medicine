import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class DataService {
    public items: any = [];

    constructor() {
        this.items = [
            { title: "one" },
            { title: "two" },
            { title: "three" },
            { title: "four" },
            { title: "five" },
            { title: "six" }
        ];
    }

    filterItems(searchTerm , items1) {
        return items1.filter(item => {
            // console.log(item);
            return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }
}
