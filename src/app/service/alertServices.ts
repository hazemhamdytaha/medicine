import {Injectable} from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    constructor(public alertCTRL: AlertController) {
    }

    public showAlert(message: string, subHeader: string , ok: string) {
        this.alertCTRL.create({
            message: message,
            subHeader: subHeader,
            buttons: [
                {text: ok,
                role : 'ok'
                }
            ]

        }).then(alert => alert.present());
    }
}