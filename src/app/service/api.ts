import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {HttpHeaders} from '@angular/common/http';

const API_STORAGE_KEY = 'specialkey';
const API_URL = 'https://reqres.in/api';

@Injectable({
    providedIn: 'root'
})


export class Api {
    apiUrl = 'https://www.pharmaclouds.net/_api/';

    constructor(private http: HttpClient, private storage: Storage) {
    }

  customerRegister(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerRegister', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerLogin(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerLogin', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerNewOrderProcess(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerNewOrderProcess', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerOrders(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerOrders', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  sliders(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'sliders', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerUpdateProfile(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerUpdateProfile', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerProfile(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerProfile', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  products(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'products', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  countries(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'countries', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerAddFavourite(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerAddFavourite', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerFavourites(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerFavourites', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerRemoveFavourite(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerRemoveFavourite', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerCart(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerCart', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerAddEditCart(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerAddEditCart', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerRemoveCart(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerRemoveCart', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerAddresses(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerAddresses', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerDelAddress(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerDelAddress', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerNewAddress(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerNewAddress', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  regions(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'regions', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }
  customerNewOrder(object: any) {
            const headers = new HttpHeaders();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

            const data = JSON.stringify(object);
            console.log(data);
            return new Promise((resolve, reject) => {
                this.http.post(this.apiUrl + 'customerNewOrder', data, {headers: headers})
                    .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    }, error => {
                        console.log(error);
                        reject(error);
                    });
            });
    }

}
