import {ApplicationRef, Injectable} from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer/ngx";
import {File} from "@ionic-native/file/ngx";

@Injectable({
    providedIn: 'root'
})
export class UploadSerive {
    uploadUrl = 'https://ac.gt4host.com/royal_app/full/api/uploadFile';
    constructor(public alertCTRL: AlertController,
                private router: Router ,
                private camera: Camera ,
                private app: ApplicationRef,
                private transfer: FileTransfer,
                private file: File,

    ) {
    }
    opencamera(type?: any){
        // type: camera.PictureSourceType.PHOTOLIBRARY or
        // camera.PictureSourceType.CAMERA
        return new Promise((resolve, reject) => {
            const options: CameraOptions = {
                quality: 5,
                sourceType: type,
                destinationType: this.camera.DestinationType.FILE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE
            }

            this.camera.getPicture(options).then((imageData) => {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64 (DATA_URL):
              //  console.log(imageData);
                resolve(imageData) ;
                // let base64Image = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {
                reject(err);
                // Handle error
            });
        });
    }
        uploadHandler(filemain: any, type?: any , index?:any ) {
            return new Promise((resolve, reject) => {

                if (type == null) {
                    type = 'image/png';
                }
                let ex = '';
                if (type === 'application/pdf') {
                    ex = 'pdf';
                } else if (type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    ex = 'docx';
                } else if (type === 'image/png') {
                    ex = 'png';
                } else if (type === 'image/jpeg') {
                    ex = 'jpeg';
                } else if (type === 'image/gif') {
                    ex = 'gif';
                } else {
                    ex = 'jpeg';
                }
                const fileTransfer = this.transfer.create();
                const options: FileUploadOptions = {
                    fileKey: 'file',
                    fileName: 'dd.' + ex,
                    params : { array_index: index},
                    chunkedMode: false
                }
                this.app.tick();
                fileTransfer.upload(filemain, this.uploadUrl, options, true).then((res) => {
                    console.log('file uploaded successfully.', res);
                    resolve(res);
                    this.app.tick();
                }).catch((error) => {
                    // here logging an error.
                    this.app.tick();
                    reject(error);
                    console.log('upload failed: ' + JSON.stringify(error));
                });
            });
        }

    // resize(uri:any){
    //     let options = {
    //         uri: uri,
    //         folderName: 'Protonet',
    //         quality: 50,
    //         width: 800,
    //         height: 800
    //     } as ImageResizerOptions;
    //     return new Promise((resolve, reject) => {
    //         this.imageResizer
    //             .resize(options)
    //             .then((filePath: string) => {
    //                 console.log('FilePath', filePath);
    //                 resolve(filePath);
    //             })
    //             .catch(e => console.log(e));
    //     });
    // }
}
