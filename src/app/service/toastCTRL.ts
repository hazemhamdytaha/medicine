import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToastCTRL {
    constructor(public toastController: ToastController) {
    }

    async presentToastWithOptions(message: string, button: boolean, postiont: any, btntxt: string , duration: number) {
        const toast = await this.toastController.create({
            message: message,
            position: postiont,
            cssClass: 'toastStyle',
            buttons: [
                {
                    side: 'start',
                    icon: 'close',
                    text: 'اغلاق',
                    cssClass: 'toastButton',
                    handler: () => {
                        console.log('Favorite clicked');
                    }
                }
            ],
            duration: 4000

        });
        toast.present();
    }
    async simple(message: string,) {
        const toast = await this.toastController.create({
            message: message,
            position: 'top',
            cssClass: 'toastStyle',
            color: 'dark',
            buttons: [
                {
                    side: 'start',
                    icon: 'close',
                    text: 'close',
                    cssClass: 'toastButton',
                    handler: () => {
                        console.log('Favorite clicked');
                    }
                }
            ],
            duration: 6000

        });
        toast.present();
    }
    async simple1(message: string,) {
        const toast = await this.toastController.create({
            message: message,
            position: 'top',
            cssClass: 'toastStyle',
            color: 'dark',
            buttons: [
                {
                    side: 'start',
                    icon: 'close',
                    text: 'close',
                    cssClass: 'toastButton',
                    handler: () => {
                        console.log('Favorite clicked');
                    }
                }
            ],
            duration: 10000

        });
        toast.present();
    }
}
