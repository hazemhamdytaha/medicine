import {ApplicationRef, Injectable} from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer/ngx";
import {File} from "@ionic-native/file/ngx";
import {DocumentViewerOptions} from "@ionic-native/document-viewer/ngx";
import {HTTP} from "@ionic-native/http/ngx";

@Injectable({
    providedIn: 'root'
})
export class Downloadservice {
    constructor(public alertCTRL: AlertController,
                private router: Router ,
                private camera: Camera ,
                private app: ApplicationRef,
                private transfer: FileTransfer,
                private http: HTTP,
                private file: File,

    ) {}
    public download(url){
            const options: DocumentViewerOptions = {
                title: 'My PDF'
            }
        return new Promise((resolve, reject) => {
            this.http.downloadFile(url, '', '', this.file.cacheDirectory + '/' + url.replace(/^.*[\\\/]/, '')).then((data: any) => {
                console.log(data);
                console.log(url.replace(/^.*[\\\/]/, ''));

                const buffer = this.file.readAsArrayBuffer(this.file.cacheDirectory, data.name).then(buffer => {
                    console.log('======>>>>>>', buffer);
                    console.log('======>>>>>>', this.file.cacheDirectory);
                    const fileBlob = new Blob([buffer], {type: 'application/pdf'});
                    console.log('======>>>>>>', fileBlob);
                    resolve(fileBlob) ;
                    // this.pdfViewerAutoLoad.pdfSrc = fileBlob; // pdfSrc can be Blob or Uint8Array
                    // this.pdfViewerAutoLoad.refresh(); // Ask pdf viewer to load/refresh pdf
                });
                // this.document.viewDocument(data.nativeURL, 'application/pdf', options);

            });
        });
    }
}
